FROM openjdk:15-jdk-alpine3.11
MAINTAINER Albert Atchurey <albert@financemobile.app>
ADD target/bpstores.jar bpstores.jar
ENTRYPOINT ["java", "-jar", "/bpstores.jar", "--spring.profiles.active=staging"]
EXPOSE 8082